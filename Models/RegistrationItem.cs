using System;

namespace triggerFoodsApi.Models
{
    public class RegistrationItem
    {
        public long Id { get; set; }   
        public string Type { get; set; }
        public DateTime Time { get; set; }

        public string MainCategoryId { get; set; }
        public string SubCategoryId { get; set; }       
        public string MainCategoryNameNorwegian { get; set; }                 
        public string MainCategoryNameEnglish { get; set; }    
        public string SubCategoryNameNorwegian { get; set; }                 
        public string SubCategoryNameEnglish { get; set; }                 
        public string NameNorwegian { get; set; }
        public string NameEnglish { get; set; }

        public int Score { get; set; }  
        public bool Pain { get; set; }          
    }
}