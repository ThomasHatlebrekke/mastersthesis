namespace triggerFoodsApi.Models
{
    public class FoodItemSuggestion
    {
        public long Id { get; set; }   
        public string NameNorwegian {get; set;}
        public string NameEnglish {get; set;}
        public int TimesAddedTotal {get; set;}
        public int TimesAddedByUser {get; set;}
    }
}