
using System.Collections.Generic;
using triggerFoodsApi.Models;

namespace triggerFoodsApi.Models
{
    public class User
    {
        public long Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }        
    }

    public class UserLogin
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}