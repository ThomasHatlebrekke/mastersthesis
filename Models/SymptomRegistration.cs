using System;
using System.Collections.Generic;
using triggerFoodsApi.Models;

namespace triggerFoodsApi.Models
{
    public class SymptomRegistration
    {
        public long Id { get; set; }
        public DateTime TimeRegistered {get; set;}
        public DateTime Time {get; set;}
        public SymptomType? SymptomType {get; set;}
        public int Score {get; set;}
        public bool? Pain {get; set;}    
        public long? UserId {get;set;}
        public User User {get; set;}        
    }

    public class SymptomRegistrationDto
    {
        public DateTime TimePerceived {get; set;}
        public int Score {get; set;}
        public SymptomType? SymptomType {get; set;}
        public string UserIdentificator {get;set;}
    }

    public enum SymptomType {
        Diarrhea,
        Constipation
    }
}