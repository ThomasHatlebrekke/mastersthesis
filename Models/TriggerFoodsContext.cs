using Microsoft.EntityFrameworkCore;
using triggerFoodsApi.Models;

namespace triggerFoodsApi.Models
{
    public class TriggerFoodsContext : DbContext
    {
        public TriggerFoodsContext(DbContextOptions<TriggerFoodsContext> options)
            : base(options)
        {
        }

        public DbSet<FoodItem> FoodItems { get; set; }
        public DbSet<FoodRegistration> FoodRegistrations { get; set; }
        public DbSet<SymptomRegistration> SymptomRegistrations { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }        
    }
}