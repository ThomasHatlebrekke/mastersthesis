using System;
using System.Collections.Generic;
using triggerFoodsApi.Models;

namespace triggerFoodsApi.Models
{
    public class FoodRegistration
    {
        public long Id { get; set; }
        public DateTime TimeRegistered { get; set; }
        public DateTime Time { get; set; }
        public long FoodItemId { get; set; }
        public FoodItem FoodItem { get; set; }        
        public float? ConsumationTime { get; set; }
        public int? AmountGrams { get; set; }
        public long? UserId { get; set; }
        public User User { get; set; }

    }

    public class FoodRegistrationDto
    {
        public DateTime TimeConsumed { get; set; }
        public long FoodItemId { get; set; }
        public string UserIdentificator {get;set;}
    }
}