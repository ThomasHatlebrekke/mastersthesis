namespace triggerFoodsApi.Models
{
    public class FoodItem
    {
        public long Id { get; set; }        
        public string MatvaretabellenId {get; set;}
        public string MainCategoryId { get; set; }
        public string SubCategoryId { get; set; }       
        public string MainCategoryNameNorwegian { get; set; }                 
        public string MainCategoryNameEnglish { get; set; }    
        public string SubCategoryNameNorwegian { get; set; }                 
        public string SubCategoryNameEnglish { get; set; }  
        public string SubSubCategoryNameEnglish { get; set; }                 
        public string NameNorwegian {get; set;}
        public string NameEnglish {get; set;}
        
        public int EdiblePart { get; set; }                 // Percent
        public string Category {get; set;}
        public double Water { get; set; }                   // Gram
        public double Kilojoules { get; set; }              // kJ
        public double Kilocalories { get; set; }            // kcal
        public double Fat { get; set; }                     // Gram
        public double SatFa { get; set; }                   // Gram, mettet fett
        public double C12_0 { get; set; }                   // Gram
        public double C14_0 { get; set; }                   // Gram
        public double C16_0 { get; set; }                   // Gram
        public double C18_0 { get; set; }                   // Gram
        public double TransFa { get; set; }                 // Gram
        public double MuFa { get; set; }                     // Gram, enummettet fett        
        public double C16_1_sum { get; set; }                // Gram 
        public double C18_1_sum { get; set; }                // Gram 
        public double PuFa { get; set; }                     // Gram, flerummettet fett
        public double C18_2n_6 { get; set; }                 // Gram
        public double C18_3n_3 { get; set; }                 // Gram
        public double C20_3n_3 { get; set; }                 // Gram
        public double C20_3n_6 { get; set; }                 // Gram
        public double C20_4n_3 { get; set; }                 // Gram
        public double C20_4n_6 { get; set; }                 // Gram
        public double C20_5n_3_EPA { get; set; }             // Gram
        public double C22_5n_3_DPA { get; set; }             // Gram
        public double C22_6n_3_DHA { get; set; }             // Gram
        public double Omega3 { get; set; }                   // Gram
        public double Omega6 { get; set; }                   // Gram
        public double Cholesterol { get; set; }              // Milligram
        public double Carbo { get; set; }                    // Gram
        public double Starch { get; set; }                   // Gram, stivelse
        public double MonoDi { get; set; }                   // Gram
        public double Sugar { get; set; }                    // Gram
        public double DietaryFibre { get; set; }             // Gram
        public double Protein { get; set; }                  // Gram
        public double Salt { get; set; }                     // Gram
        public double Alcohol { get; set; }                  // Gram
        public double VitaminA { get; set; }                 // RAE
        public double Betacarotene { get; set; }             // Microgram
        public double Retinol { get; set; }                  // Microgram
        public double VitaminD { get; set; }                 // Microgram
        public double VitaminE { get; set; }                 // alfa-TE
        public double Thiamin { get; set; }                  // Milligram
        public double Riboflavin { get; set; }               // Milligram
        public double Niacin { get; set; }                   // Milligram
        public double VitaminB6 { get; set; }                // Milligram
        public double Folate { get; set; }                   // Microgram
        public double VitaminB12 { get; set; }               // Microgram
        public double VitaminC { get; set; }                 // Milligram
        public double Calcium { get; set; }                  // Milligram
        public double Iron { get; set; }                     // Milligram
        public double Sodium { get; set; }                   // Milligram
        public double Potassium { get; set; }                // Milligram
        public double Magnesium { get; set; }                // Milligram
        public double Zinc { get; set; }                     // Milligram
        public double Selenium { get; set; }                 // Microgram
        public double Copper { get; set; }                   // Milligram
        public double Phosphorus { get; set; }               // Milligram
        public double Iodine { get; set; }                   // Microgram
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        

        
        
        
        
        
    
        
        
        
        
                


    }
}