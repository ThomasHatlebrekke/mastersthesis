using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using triggerFoodsApi.Models;

namespace triggerFoodsApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class FoodItemController : ControllerBase
    {
        private readonly TriggerFoodsContext _context;
        private IHostingEnvironment _hostingEnvironment;
        private readonly IHttpClientFactory _clientFactory;

        public FoodItemController(TriggerFoodsContext context, IHostingEnvironment hostingEnvironment, IHttpClientFactory clientFactory)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _clientFactory = clientFactory;

            // Go through Norwegian Excel-file and add Norwegian names based on MatvaretabellenId in DB
            //this.fillNorwegian();           

            if (_context.FoodItems.Count() == 0)
            {
                // Create new FoodItems if collection is empty,
                // which means you can't delete all FoodItems.

                // Go through English Excel-file and add all Food items                
                this.fillDatabase();     
                this.fillNorwegian();      
                this.fillSubSubCategoryNameEnglish();     
            }
        }

        [HttpDelete("foodregistration/{id}")]
        public async Task<IActionResult> DeleteFoodRegistration([FromRoute] int id) 
        {
            try 
            {
                var reg = _context.FoodRegistrations.FirstOrDefault(x => x.Id == id && x.UserId == Convert.ToInt64(User.Identity.Name));
                _context.FoodRegistrations.Remove(reg);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch(Exception e)
            {
                return BadRequest(e.InnerException.Message);
            }
        }        

        [HttpPost("foodregistration")]
        public async Task<IActionResult> PostFoodRegistration([FromBody] List<FoodRegistrationDto> foodRegistrations) 
        {

            if (foodRegistrations != null && foodRegistrations.Count > 0) 
            {
                for (var i = 0; i < foodRegistrations.Count; i++) 
                {
                    if (foodRegistrations[i].TimeConsumed != null)
                    {
                        try 
                        {
                            var newFoodRegistration = new FoodRegistration 
                            {
                                Time = foodRegistrations[i].TimeConsumed,
                                TimeRegistered = DateTime.Now,
                                UserId = Convert.ToInt64(User.Identity.Name),
                                FoodItemId = foodRegistrations[i].FoodItemId
                            };
                            var x = _context.FoodRegistrations.Add(newFoodRegistration);
                            if (x == null)
                            {
                                return BadRequest("Food registration object could not be added");
                            }
                        }
                        catch(Exception e)
                        {
                            return BadRequest(e.InnerException.Message);
                        }
                    }
                }
                try 
                {
                    await _context.SaveChangesAsync();
                    return Ok();
                } 
                catch(Exception e)
                {
                    return BadRequest(e.Message);
                }            
                
            }
            else 
            {
                return BadRequest();
            }


            
        }

        [HttpGet("suggestions_en")]
        public async Task<IActionResult> GetSuggestionsEnglish(string keywords)
        {
            List<FoodItemSuggestion> suggestions = new List<FoodItemSuggestion>();

            string[] keys = keywords.Split(',');
            foreach (var key in keys)
            {
                // Searching for foods where keyword is in name
                var suggestionListName = _context.FoodItems.Where(f => f.NameEnglish.Contains(key));
                foreach (var suggestion in suggestionListName) 
                {
                    suggestions.Add(new FoodItemSuggestion 
                    {
                        Id = suggestion.Id,
                        NameNorwegian = suggestion.NameNorwegian,
                        NameEnglish = suggestion.NameEnglish
                    });
                }
                // Searching for foods where keyword is in main category
                var suggestionListMainCategory = _context.FoodItems.Where(f => f.MainCategoryNameEnglish.Contains(key));
                foreach (var suggestion in suggestionListMainCategory) 
                {
                    suggestions.Add(new FoodItemSuggestion 
                    {
                        Id = suggestion.Id,
                        NameNorwegian = suggestion.NameNorwegian,
                        NameEnglish = suggestion.NameEnglish
                    });
                }    
                // Searching for foods where keyword is in sub category
                var suggestionListSubCategory = _context.FoodItems.Where(f => f.SubCategoryNameEnglish.Contains(key));
                foreach (var suggestion in suggestionListSubCategory) 
                {
                    suggestions.Add(new FoodItemSuggestion 
                    {
                        Id = suggestion.Id,
                        NameNorwegian = suggestion.NameNorwegian,
                        NameEnglish = suggestion.NameEnglish
                    });
                }                                
            }   
            suggestions = suggestions.Distinct().ToList();         
            return Ok(suggestions);
        }

        [HttpGet("suggestions_no")]
        public async Task<IActionResult> GetSuggestionsNorwegian(string keywords)
        {
            List<FoodItemSuggestion> suggestions = new List<FoodItemSuggestion>();

            string[] keys = keywords.Split(',');
            foreach (var key in keys)
            {
                // Searching for foods where keyword is in name
                var suggestionListName = _context.FoodItems.Where(f => f.NameNorwegian.Contains(key));
                foreach (var suggestion in suggestionListName) 
                {
                    suggestions.Add(new FoodItemSuggestion 
                    {
                        Id = suggestion.Id,
                        NameNorwegian = suggestion.NameNorwegian,
                        NameEnglish = suggestion.NameEnglish
                    });
                }
                // Searching for foods where keyword is in main category
                var suggestionListMainCategory = _context.FoodItems.Where(f => f.MainCategoryNameNorwegian.Contains(key));
                foreach (var suggestion in suggestionListMainCategory) 
                {
                    suggestions.Add(new FoodItemSuggestion 
                    {
                        Id = suggestion.Id,
                        NameNorwegian = suggestion.NameNorwegian,
                        NameEnglish = suggestion.NameEnglish
                    });
                }    
                // Searching for foods where keyword is in sub category
                var suggestionListSubCategory = _context.FoodItems.Where(f => f.SubCategoryNameNorwegian.Contains(key));
                foreach (var suggestion in suggestionListSubCategory) 
                {
                    suggestions.Add(new FoodItemSuggestion 
                    {
                        Id = suggestion.Id,
                        NameNorwegian = suggestion.NameNorwegian,
                        NameEnglish = suggestion.NameEnglish
                    });
                }                                
            }   
            suggestions = suggestions.Distinct().ToList();         
            return Ok(suggestions);
        }        

        [HttpGet]
        public ActionResult<List<FoodItem>> GetAll()
        {
            return _context.FoodItems.ToList();
        }    

        [HttpGet("{id}", Name = "GetFoodItem")]
        public ActionResult<FoodItem> GetById(long id)
        {
            var item = _context.FoodItems.Find(id);
            if (item == null)
            {
                return NotFound();
            }
            return item;
        }     

        public void fillNorwegian() 
        {
            string fileName = @"/Users/thomas/Dev/triggerFoodsApi/wwwroot/Matvaretabellen_NO.csv";
        
            using(var reader = new StreamReader(fileName))
            {       
                      
                string mainCatId = "";
                string mainCatName = "";
                string subCatId = "";
                string subCatName = "";                                
            
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var l = line.Split(';');
                    
                    int i = 0; 
                    double d = 0;

                    string idValue = l[0];
                    string nameValue = l[1];
                    string edibleValue = l[2];  
                    bool isEdibleNumber = int.TryParse(edibleValue, out i);
                    bool isIdInteger = int.TryParse(idValue, out i);
                    bool isIdDouble = double.TryParse(idValue, out d);

                    bool isFoodItem = false;                    
                    // Checking if line is a food item
                    if (isEdibleNumber || edibleValue == "M") 
                    {
                        isFoodItem = true;
                    } 
                    else if (isIdInteger)
                    {
                        mainCatId = idValue;
                        mainCatName = nameValue;
                    }
                    else if (isIdDouble)
                    {
                        subCatId = idValue;
                        subCatName = nameValue;
                    } 

                    if (isFoodItem)
                    {
                        var result = _context.FoodItems.SingleOrDefault(f => f.MatvaretabellenId == idValue);
                        if (result != null)
                        {                          
                            result.MainCategoryNameNorwegian = mainCatName;
                            result.SubCategoryNameNorwegian = subCatName;
                            result.NameNorwegian = nameValue;         
                            _context.SaveChanges();                                                 
                        }  
                    }        
                }
            } 
                    
        }

        public void fillSubSubCategoryNameEnglish()
        {
            string fileName = @"/Users/thomas/Dev/triggerFoodsApi/wwwroot/Matvaretabellen_EN-kopi.csv";
        
            using(var reader = new StreamReader(fileName))
            {       
                      
                string mainCatId = "";
                string mainCatName = "";
                string subCatId = "";
                string subCatName = "";      
                string subSubCatName = "";          
            
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var l = line.Split(';');
                    
                    int i = 0; 
                    double d = 0;

                    string idValue = l[0];
                    string nameValue = l[1];
                    string edibleValue = l[2];  
                    bool isEdibleNumber = int.TryParse(edibleValue, out i);
                    bool isIdInteger = int.TryParse(idValue, out i);
                    bool isIdDouble = double.TryParse(idValue, out d);

                    bool isFoodItem = false;                    
                    // Checking if line is a food item
                    if (isEdibleNumber || edibleValue == "M") 
                    {
                        isFoodItem = true;
                    } 
                    else
                    {
                        subSubCatName = nameValue;
                    }

                    if (isFoodItem) 
                    {
                        var foodItem = _context.FoodItems.SingleOrDefault(f => f.MatvaretabellenId == idValue);
                        if (foodItem != null)
                        {
                            foodItem.SubSubCategoryNameEnglish = subSubCatName;
                            _context.SaveChanges();
                        }                                                            
                    }
                }
            }                           
        }
        public void fillDatabase() 
        {
            string fileName = @"/Users/thomas/Dev/triggerFoodsApi/wwwroot/Matvaretabellen_EN-kopi.csv";
        
            using(var reader = new StreamReader(fileName))
            {       
                      
                string mainCatId = "";
                string mainCatName = "";
                string subCatId = "";
                string subCatName = "";                
            
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var l = line.Split(';');
                    
                    int i = 0; 
                    double d = 0;

                    string idValue = l[0];
                    string nameValue = l[1];
                    string edibleValue = l[2];  
                    bool isEdibleNumber = int.TryParse(edibleValue, out i);
                    bool isIdInteger = int.TryParse(idValue, out i);
                    bool isIdDouble = double.TryParse(idValue, out d);

                    bool isFoodItem = false;                    
                    // Checking if line is a food item
                    if (isEdibleNumber || edibleValue == "M") 
                    {
                        isFoodItem = true;
                    } 
                    else if (isIdInteger)
                    {
                        mainCatId = idValue;
                        mainCatName = nameValue;
                    }
                    else if (isIdDouble)
                    {
                        subCatId = idValue;
                        subCatName = nameValue;
                    }  

                    if (isFoodItem) 
                    {
                        FoodItem f = new FoodItem
                        {
                            MatvaretabellenId = idValue,
                            MainCategoryId = mainCatId,
                            MainCategoryNameEnglish = mainCatName,
                            SubCategoryId = subCatId,
                            SubCategoryNameEnglish = subCatName,
                            NameEnglish = nameValue,
                            EdiblePart = int.TryParse(l[2], out i) ? i : -1,
                            Water = double.TryParse(l[4], out d) ? d : -1,
                            Kilojoules = double.TryParse(l[6], out d) ? d : -1,
                            Kilocalories = double.TryParse(l[8], out d) ? d : -1,
                            Fat = double.TryParse(l[10], out d) ? d : -1,
                            SatFa = double.TryParse(l[12], out d) ? d : -1,
                            C12_0 = double.TryParse(l[14], out d) ? d : -1,
                            C14_0 = double.TryParse(l[16], out d) ? d : -1,
                            C16_0 = double.TryParse(l[18], out d) ? d : -1,
                            C18_0 = double.TryParse(l[20], out d) ? d : -1,
                            TransFa = double.TryParse(l[22], out d) ? d : -1,
                            MuFa = double.TryParse(l[24], out d) ? d : -1,
                            C16_1_sum = double.TryParse(l[26], out d) ? d : -1,
                            C18_1_sum = double.TryParse(l[28], out d) ? d : -1,
                            PuFa = double.TryParse(l[30], out d) ? d : -1,
                            C18_2n_6 = double.TryParse(l[32], out d) ? d : -1,
                            C18_3n_3 = double.TryParse(l[34], out d) ? d : -1,
                            C20_3n_3 = double.TryParse(l[36], out d) ? d : -1,
                            C20_3n_6 = double.TryParse(l[38], out d) ? d : -1,
                            C20_4n_3 = double.TryParse(l[40], out d) ? d : -1,
                            C20_4n_6 = double.TryParse(l[42], out d) ? d : -1,
                            C20_5n_3_EPA = double.TryParse(l[44], out d) ? d : -1,
                            C22_5n_3_DPA = double.TryParse(l[46], out d) ? d : -1,
                            C22_6n_3_DHA = double.TryParse(l[48], out d) ? d : -1,
                            Omega3 = double.TryParse(l[50], out d) ? d : -1,
                            Omega6 = double.TryParse(l[52], out d) ? d : -1,
                            Cholesterol = double.TryParse(l[54], out d) ? d : -1,
                            Carbo = double.TryParse(l[56], out d) ? d : -1,
                            Starch = double.TryParse(l[58], out d) ? d : -1,
                            MonoDi = double.TryParse(l[60], out d) ? d : -1,
                            Sugar = double.TryParse(l[62], out d) ? d : -1,
                            DietaryFibre = double.TryParse(l[64], out d) ? d : -1,
                            Protein = double.TryParse(l[66], out d) ? d : -1,
                            Salt = double.TryParse(l[68], out d) ? d : -1,
                            Alcohol = double.TryParse(l[70], out d) ? d : -1,
                            VitaminA = double.TryParse(l[72], out d) ? d : -1,
                            Betacarotene = double.TryParse(l[74], out d) ? d : -1,
                            Retinol = double.TryParse(l[76], out d) ? d : -1,
                            VitaminD = double.TryParse(l[78], out d) ? d : -1,
                            VitaminE = double.TryParse(l[80], out d) ? d : -1,
                            Thiamin = double.TryParse(l[82], out d) ? d : -1,
                            Riboflavin = double.TryParse(l[84], out d) ? d : -1,
                            Niacin = double.TryParse(l[86], out d) ? d : -1,
                            VitaminB6 = double.TryParse(l[88], out d) ? d : -1,
                            Folate = double.TryParse(l[90], out d) ? d : -1,
                            VitaminB12 = double.TryParse(l[92], out d) ? d : -1,
                            VitaminC = double.TryParse(l[94], out d) ? d : -1,
                            Calcium = double.TryParse(l[96], out d) ? d : -1,
                            Iron = double.TryParse(l[98], out d) ? d : -1,
                            Sodium = double.TryParse(l[100], out d) ? d : -1,
                            Potassium = double.TryParse(l[102], out d) ? d : -1,
                            Magnesium = double.TryParse(l[104], out d) ? d : -1,
                            Zinc = double.TryParse(l[106], out d) ? d : -1,
                            Selenium = double.TryParse(l[108], out d) ? d : -1,
                            Copper = double.TryParse(l[110], out d) ? d : -1,
                            Phosphorus = double.TryParse(l[112], out d) ? d : -1,
                            Iodine = double.TryParse(l[114], out d) ? d : -1
                        };

                        //Debug.WriteLine(f.MainCategoryNameEnglish + " ------> " + f.SubCategoryNameEnglish);
                        //Debug.WriteLine(f.MatvaretabellenId + ": " + f.NameEnglish + ", " + f.EdiblePart + ", " + f.MainCategoryNameEnglish + ", " + f.SubCategoryNameEnglish);
                        _context.FoodItems.Add(f);
                                        
                    }
                }
            }
            _context.SaveChanges();        
        }    

    }
}