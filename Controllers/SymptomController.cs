using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using triggerFoodsApi.Models;

namespace triggerFoodsApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SymptomController : ControllerBase
    {
        private readonly TriggerFoodsContext _context;
        private IHostingEnvironment _hostingEnvironment;
        private readonly IHttpClientFactory _clientFactory;

        public SymptomController(TriggerFoodsContext context, IHostingEnvironment hostingEnvironment, IHttpClientFactory clientFactory)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _clientFactory = clientFactory;
        }
        
        [HttpDelete("symptomregistration/{id}")]
        public async Task<IActionResult> DeleteSymptomRegistration([FromRoute] int id) 
        {
            try 
            {
                var reg = _context.SymptomRegistrations.FirstOrDefault(x => x.Id == id && x.UserId == Convert.ToInt64(User.Identity.Name));
                _context.SymptomRegistrations.Remove(reg);
                await _context.SaveChangesAsync();
                return Ok();
            }
            catch(Exception e)
            {
                return BadRequest(e.InnerException.Message);
            }
        }         

        [HttpPost("symptomregistration")]
        public async Task<IActionResult> PostSymptomRegistration([FromBody] SymptomRegistrationDto symptomRegistration) 
        {
            
            if (symptomRegistration != null &&
                symptomRegistration.TimePerceived != null &&
                symptomRegistration.Score > 0 &&
                symptomRegistration.Score <= 500) 
            {
                try 
                {
                    var newSymptomRegistration = new SymptomRegistration 
                    {
                        Time = symptomRegistration.TimePerceived,
                        TimeRegistered = DateTime.Now,
                        Score = symptomRegistration.Score,
                        UserId = Convert.ToInt64(User.Identity.Name)
                        //SymptomType = symptomRegistration.SymptomType
                    };
                    var x = _context.SymptomRegistrations.Add(newSymptomRegistration);
                    if (x == null)
                    {
                        return BadRequest("Food registration object could not be added");
                    }
                    await _context.SaveChangesAsync();
                    return Ok();
                }
                catch(Exception e)
                {
                    return BadRequest(e.InnerException.Message);
                }                
            }
            else 
            {
                return BadRequest();
            }   
        }        
    }
}