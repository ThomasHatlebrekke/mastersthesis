using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using triggerFoodsApi.Models;

namespace triggerFoodsApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]    
    public class HistoryController : ControllerBase
    {
        private readonly TriggerFoodsContext _context;
        private IHostingEnvironment _hostingEnvironment;
        //private IVisualRecognitionService _visualRecognition;
        private readonly IHttpClientFactory _clientFactory;

        public HistoryController(TriggerFoodsContext context, IHostingEnvironment hostingEnvironment, IHttpClientFactory clientFactory)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _clientFactory = clientFactory;
        }

        [HttpGet("")]
        public async Task<IActionResult> GetSRegistrationHistory() 
        {            
            try 
            {
                var userId = Convert.ToInt64(User.Identity.Name);

                // Only get from logged in user
                var fRegs = _context.FoodRegistrations.Where(x=> x.UserId == userId).Include(reg => reg.FoodItem).ToList();
                var sRegs = _context.SymptomRegistrations.Where(x=> x.UserId == userId).ToList();

                List<RegistrationItem> l = new List<RegistrationItem>();

                for (var i = 0; i < fRegs.Count; i++)
                {

                    var r = new RegistrationItem();
                    r.Id = fRegs[i].Id;
                    r.Time = fRegs[i].Time;
                    r.Type = "FoodItem";
                    r.NameNorwegian = fRegs[i].FoodItem.NameNorwegian;
                    l.Add(r);
                }
                for (var i = 0; i < sRegs.Count; i++)
                {
                    var r = new RegistrationItem {
                        Id = sRegs[i].Id,
                        Time = sRegs[i].Time,
                        Type = "Symptom",
                        Score = sRegs[i].Score
                    };
                    l.Add(r);
                }
                
                //dynamicList.OrderBy(x => x.Time);
                l.Sort((y, x) => DateTime.Compare(x.Time, y.Time));
                return Ok(l);
            }
            catch(Exception e)
            {
                return BadRequest(e.InnerException.Message);
            }                

        }        
    }
}