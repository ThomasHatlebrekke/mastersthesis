using triggerFoodsApi.Models;

public class ProcessService
    {
        // users hardcoded for simplicity, store in a db with hashed passwords in production applications
        // private List<User> _users = new List<User>
        // { 
        //     new User { Id = 1, FirstName = "Test", LastName = "User", Username = "test", Password = "test" } 
        // };

        private readonly TriggerFoodsContext _context;

        public ProcessService(TriggerFoodsContext context)
        {
            _context = context;
        }

        public void Process() 
        {
            System.Diagnostics.Debug.WriteLine("SomeText");
        }

        

}