﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace triggerFoodsApi.Migrations
{
    public partial class TimeFieldChanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SymptomRegistrations_User_UserId",
                table: "SymptomRegistrations");

            migrationBuilder.RenameColumn(
                name: "TimePerceived",
                table: "SymptomRegistrations",
                newName: "Time");

            migrationBuilder.RenameColumn(
                name: "TimeConsumed",
                table: "FoodRegistrations",
                newName: "Time");

            migrationBuilder.AlterColumn<long>(
                name: "UserId",
                table: "SymptomRegistrations",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<int>(
                name: "SymptomType",
                table: "SymptomRegistrations",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<bool>(
                name: "Pain",
                table: "SymptomRegistrations",
                nullable: true,
                oldClrType: typeof(bool));

            migrationBuilder.AddForeignKey(
                name: "FK_SymptomRegistrations_User_UserId",
                table: "SymptomRegistrations",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SymptomRegistrations_User_UserId",
                table: "SymptomRegistrations");

            migrationBuilder.RenameColumn(
                name: "Time",
                table: "SymptomRegistrations",
                newName: "TimePerceived");

            migrationBuilder.RenameColumn(
                name: "Time",
                table: "FoodRegistrations",
                newName: "TimeConsumed");

            migrationBuilder.AlterColumn<long>(
                name: "UserId",
                table: "SymptomRegistrations",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "SymptomType",
                table: "SymptomRegistrations",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "Pain",
                table: "SymptomRegistrations",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_SymptomRegistrations_User_UserId",
                table: "SymptomRegistrations",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
