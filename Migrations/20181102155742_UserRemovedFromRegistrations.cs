﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace triggerFoodsApi.Migrations
{
    public partial class UserRemovedFromRegistrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FoodRegistrations_Users_UserId",
                table: "FoodRegistrations");

            migrationBuilder.DropForeignKey(
                name: "FK_SymptomRegistrations_Users_UserId",
                table: "SymptomRegistrations");

            migrationBuilder.DropIndex(
                name: "IX_SymptomRegistrations_UserId",
                table: "SymptomRegistrations");

            migrationBuilder.DropIndex(
                name: "IX_FoodRegistrations_UserId",
                table: "FoodRegistrations");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "SymptomRegistrations");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "FoodRegistrations");

            migrationBuilder.AddColumn<string>(
                name: "UserIdentificator",
                table: "SymptomRegistrations",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "UserIdentificator",
                table: "FoodRegistrations",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "UserIdentificator",
                table: "SymptomRegistrations");

            migrationBuilder.DropColumn(
                name: "UserIdentificator",
                table: "FoodRegistrations");

            migrationBuilder.AddColumn<long>(
                name: "UserId",
                table: "SymptomRegistrations",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "UserId",
                table: "FoodRegistrations",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SymptomRegistrations_UserId",
                table: "SymptomRegistrations",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_FoodRegistrations_UserId",
                table: "FoodRegistrations",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_FoodRegistrations_Users_UserId",
                table: "FoodRegistrations",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SymptomRegistrations_Users_UserId",
                table: "SymptomRegistrations",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
