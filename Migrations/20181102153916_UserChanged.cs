﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace triggerFoodsApi.Migrations
{
    public partial class UserChanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FoodRegistrations_User_UserId",
                table: "FoodRegistrations");

            migrationBuilder.DropForeignKey(
                name: "FK_SymptomRegistrations_User_UserId",
                table: "SymptomRegistrations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_User",
                table: "User");

            migrationBuilder.DropColumn(
                name: "Age",
                table: "User");

            migrationBuilder.RenameTable(
                name: "User",
                newName: "Users");

            migrationBuilder.RenameColumn(
                name: "Sex",
                table: "Users",
                newName: "Username");

            migrationBuilder.RenameColumn(
                name: "Lastname",
                table: "Users",
                newName: "Token");

            migrationBuilder.RenameColumn(
                name: "Firstname",
                table: "Users",
                newName: "Password");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Users",
                table: "Users",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_FoodRegistrations_Users_UserId",
                table: "FoodRegistrations",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SymptomRegistrations_Users_UserId",
                table: "SymptomRegistrations",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FoodRegistrations_Users_UserId",
                table: "FoodRegistrations");

            migrationBuilder.DropForeignKey(
                name: "FK_SymptomRegistrations_Users_UserId",
                table: "SymptomRegistrations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Users",
                table: "Users");

            migrationBuilder.RenameTable(
                name: "Users",
                newName: "User");

            migrationBuilder.RenameColumn(
                name: "Username",
                table: "User",
                newName: "Sex");

            migrationBuilder.RenameColumn(
                name: "Token",
                table: "User",
                newName: "Lastname");

            migrationBuilder.RenameColumn(
                name: "Password",
                table: "User",
                newName: "Firstname");

            migrationBuilder.AddColumn<int>(
                name: "Age",
                table: "User",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_User",
                table: "User",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_FoodRegistrations_User_UserId",
                table: "FoodRegistrations",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SymptomRegistrations_User_UserId",
                table: "SymptomRegistrations",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
