﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace triggerFoodsApi.Migrations
{
    public partial class FoodAndSymptomRegistrations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Firstname = table.Column<string>(nullable: true),
                    Lastname = table.Column<string>(nullable: true),
                    Sex = table.Column<string>(nullable: true),
                    Age = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FoodRegistrations",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TimeRegistered = table.Column<DateTime>(nullable: false),
                    TimeConsumed = table.Column<DateTime>(nullable: false),
                    ConsumationTime = table.Column<float>(nullable: false),
                    AmountGrams = table.Column<int>(nullable: false),
                    UserId = table.Column<long>(nullable: false),
                    FoodItemId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FoodRegistrations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FoodRegistrations_FoodItems_FoodItemId",
                        column: x => x.FoodItemId,
                        principalTable: "FoodItems",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FoodRegistrations_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "SymptomRegistrations",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    TimeRegistered = table.Column<DateTime>(nullable: false),
                    TimePerceived = table.Column<DateTime>(nullable: false),
                    SymptomType = table.Column<int>(nullable: false),
                    Score = table.Column<int>(nullable: false),
                    Pain = table.Column<bool>(nullable: false),
                    UserId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SymptomRegistrations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SymptomRegistrations_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FoodRegistrations_FoodItemId",
                table: "FoodRegistrations",
                column: "FoodItemId");

            migrationBuilder.CreateIndex(
                name: "IX_FoodRegistrations_UserId",
                table: "FoodRegistrations",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_SymptomRegistrations_UserId",
                table: "SymptomRegistrations",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FoodRegistrations");

            migrationBuilder.DropTable(
                name: "SymptomRegistrations");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
