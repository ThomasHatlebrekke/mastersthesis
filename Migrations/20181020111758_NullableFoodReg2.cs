﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace triggerFoodsApi.Migrations
{
    public partial class NullableFoodReg2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FoodRegistrations_User_UserId",
                table: "FoodRegistrations");

            migrationBuilder.AlterColumn<long>(
                name: "UserId",
                table: "FoodRegistrations",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<float>(
                name: "ConsumationTime",
                table: "FoodRegistrations",
                nullable: false,
                oldClrType: typeof(float),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AmountGrams",
                table: "FoodRegistrations",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_FoodRegistrations_User_UserId",
                table: "FoodRegistrations",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_FoodRegistrations_User_UserId",
                table: "FoodRegistrations");

            migrationBuilder.AlterColumn<long>(
                name: "UserId",
                table: "FoodRegistrations",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<float>(
                name: "ConsumationTime",
                table: "FoodRegistrations",
                nullable: true,
                oldClrType: typeof(float));

            migrationBuilder.AlterColumn<int>(
                name: "AmountGrams",
                table: "FoodRegistrations",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_FoodRegistrations_User_UserId",
                table: "FoodRegistrations",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
