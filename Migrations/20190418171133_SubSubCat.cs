﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace triggerFoodsApi.Migrations
{
    public partial class SubSubCat : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SubSubCategoryNameEnglish",
                table: "FoodItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SubSubCategoryNameEnglish",
                table: "FoodItems");
        }
    }
}
