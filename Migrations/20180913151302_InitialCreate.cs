﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace triggerFoodsApi.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FoodItems",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    MatvaretabellenId = table.Column<string>(nullable: true),
                    MainCategoryId = table.Column<string>(nullable: true),
                    SubCategoryId = table.Column<string>(nullable: true),
                    MainCategoryNameNorwegian = table.Column<string>(nullable: true),
                    MainCategoryNameEnglish = table.Column<string>(nullable: true),
                    SubCategoryNameNorwegian = table.Column<string>(nullable: true),
                    SubCategoryNameEnglish = table.Column<string>(nullable: true),
                    NameNorwegian = table.Column<string>(nullable: true),
                    NameEnglish = table.Column<string>(nullable: true),
                    EdiblePart = table.Column<int>(nullable: false),
                    Category = table.Column<string>(nullable: true),
                    Water = table.Column<double>(nullable: false),
                    Kilojoules = table.Column<double>(nullable: false),
                    Kilocalories = table.Column<double>(nullable: false),
                    Fat = table.Column<double>(nullable: false),
                    SatFa = table.Column<double>(nullable: false),
                    C12_0 = table.Column<double>(nullable: false),
                    C14_0 = table.Column<double>(nullable: false),
                    C16_0 = table.Column<double>(nullable: false),
                    C18_0 = table.Column<double>(nullable: false),
                    TransFa = table.Column<double>(nullable: false),
                    MuFa = table.Column<double>(nullable: false),
                    C16_1_sum = table.Column<double>(nullable: false),
                    C18_1_sum = table.Column<double>(nullable: false),
                    PuFa = table.Column<double>(nullable: false),
                    C18_2n_6 = table.Column<double>(nullable: false),
                    C18_3n_3 = table.Column<double>(nullable: false),
                    C20_3n_3 = table.Column<double>(nullable: false),
                    C20_3n_6 = table.Column<double>(nullable: false),
                    C20_4n_3 = table.Column<double>(nullable: false),
                    C20_4n_6 = table.Column<double>(nullable: false),
                    C20_5n_3_EPA = table.Column<double>(nullable: false),
                    C22_5n_3_DPA = table.Column<double>(nullable: false),
                    C22_6n_3_DHA = table.Column<double>(nullable: false),
                    Omega3 = table.Column<double>(nullable: false),
                    Omega6 = table.Column<double>(nullable: false),
                    Cholesterol = table.Column<double>(nullable: false),
                    Carbo = table.Column<double>(nullable: false),
                    Starch = table.Column<double>(nullable: false),
                    MonoDi = table.Column<double>(nullable: false),
                    Sugar = table.Column<double>(nullable: false),
                    DietaryFibre = table.Column<double>(nullable: false),
                    Protein = table.Column<double>(nullable: false),
                    Salt = table.Column<double>(nullable: false),
                    Alcohol = table.Column<double>(nullable: false),
                    VitaminA = table.Column<double>(nullable: false),
                    Betacarotene = table.Column<double>(nullable: false),
                    Retinol = table.Column<double>(nullable: false),
                    VitaminD = table.Column<double>(nullable: false),
                    VitaminE = table.Column<double>(nullable: false),
                    Thiamin = table.Column<double>(nullable: false),
                    Riboflavin = table.Column<double>(nullable: false),
                    Niacin = table.Column<double>(nullable: false),
                    VitaminB6 = table.Column<double>(nullable: false),
                    Folate = table.Column<double>(nullable: false),
                    VitaminB12 = table.Column<double>(nullable: false),
                    VitaminC = table.Column<double>(nullable: false),
                    Calcium = table.Column<double>(nullable: false),
                    Iron = table.Column<double>(nullable: false),
                    Sodium = table.Column<double>(nullable: false),
                    Potassium = table.Column<double>(nullable: false),
                    Magnesium = table.Column<double>(nullable: false),
                    Zinc = table.Column<double>(nullable: false),
                    Selenium = table.Column<double>(nullable: false),
                    Copper = table.Column<double>(nullable: false),
                    Phosphorus = table.Column<double>(nullable: false),
                    Iodine = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FoodItems", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FoodItems");
        }
    }
}
