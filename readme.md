Source code used in Thomas Akselberg Hatlebrekke's Master's thesis "Use of Association Rule Mining to Identify Trigger Foods in Irritable Bowel Syndrome".

Four separate projects are located in the following branches:

- triggerFoodsApi (Web API)
- triggerFoodsApp (Mobile application)
- triggerFoodsPreprocess (Console application for generation of datasets)
- triggerFoodsRules (Jupyter Notebook for generation of association rules)